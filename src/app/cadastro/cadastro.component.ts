import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  form: FormGroup;
  @Output() dadosParticipante = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.setFormulario();
  }

  // Definir os dados para enviar
  setFormulario() {
    this.form = new FormGroup({
      nome: new FormControl(null, Validators.required),
      sobrenome: new FormControl(null, Validators.required),
      participacao: new FormControl(null, Validators.required)
    });
  }

  // Envia os dados e lista os participantes
  enviarDados() {
    this.dadosParticipante.emit(this.form.value);
    this.form.reset();
  }

}
