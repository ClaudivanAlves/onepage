import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, OnChanges {

  dataSource: Object;
  @Input() participantes: Array<any>;
  @Input() newParticipante;

  constructor() {
    this.dataSource = {
      chart: {
        "showpercentvalues": "0",
        "use3DLighting": "0",
        "legendposition": "bottom",
        "usedataplotcolorforlabels": "1",
        "theme": "fusion",
        "bgColor": "#FFFFFF",
        "showBorder": "0"
      }
    };    
  }

  fillChart() {
    this.dataSource["data"] = [];

    for (const i in this.participantes) {
      const dado = { "label": this.participantes[i].nome + " " + this.participantes[i].sobrenome, "value": this.participantes[i].participacao };
      this.dataSource["data"][i] = dado;
    }
  }

  ngOnInit() {
    this.fillChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.newParticipante && !changes.newParticipante.isFirstChange()) {
      const lastData = this.dataSource["data"].length;
      
      const dado = { "label": this.newParticipante.nome, "value": this.newParticipante.participacao };
      this.dataSource["data"][lastData] = dado;
    }
  }

}
