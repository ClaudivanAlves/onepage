import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-dados',
  templateUrl: './dados.component.html',
  styleUrls: ['./dados.component.css']
})
export class DadosComponent implements OnInit {

  participantes: Array<any>;
  sucessSave: boolean = false;
  totalExcedido: boolean = false;
  newParticipante;
  
  constructor() { 
    this.participantes = [
      { id: 1, nome: "Carlos", sobrenome: "Moura", participacao: 5 },
      { id: 2, nome: "Fernanda", sobrenome: "Oliveira", participacao: 15 },
      { id: 3, nome: "Hugo", sobrenome: "Silva", participacao: 20 },
      { id: 4, nome: "Eliza", sobrenome: "Souza", participacao: 20 },
    ];
  }
  ngOnInit() {
  }

  sumandoParticipacao() {
    let totalParticipacao = 0;
    
    for (let i in this.participantes) {
      totalParticipacao += this.participantes[i].participacao;
    }

    return totalParticipacao;
  }

  setarLastId() {
    let lastId = 0;
  
    for (let i in this.participantes) {
      lastId = this.participantes[i].id;
    }

    return (lastId + 1);
  }

  recieveDatas(dadosParticipante) {
    if ((this.sumandoParticipacao() + dadosParticipante.participacao) <= 100) {
      dadosParticipante.id = this.setarLastId();
      this.participantes.push(dadosParticipante);
      this.newParticipante = dadosParticipante;
      this.sucessSave = true;

      this.sumandoParticipacao();

      setTimeout(() => {
        document.getElementById("sucess").style.display = 'none';
      }, 5000);
    } else {
      this.totalExcedido = true;

      setTimeout(() => {
        document.getElementById("error").style.display = 'none';
      }, 5000);
    }
  }

}
